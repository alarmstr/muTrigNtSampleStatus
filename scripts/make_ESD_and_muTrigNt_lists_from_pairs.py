#!/usr/bin/env python

from __future__ import print_function

# Order determines ranking
KNOWN_SCOPES = ['user.alarmstr', 'group.perf-muons', 'user.asoffa', 'mc15_14TeV', 'data16_13TeV']
REQUIRED_ESD_LIST = "./data/ESDs_force_tracking.txt"

import os, sys
import argparse
def parse_args(unparsed_args):
    parser = argparse.ArgumentParser()
    parser.add_argument('ifile_path', help='Path to input json file')
    parser.add_argument('--key', help='Dictionart key for ESD-muTrigNt dictionary in input json file')
    parser.add_argument('--muTrigNt-ofile', default="muTrigNts_to_process.txt", help='Path for output file of muTrigNts')
    parser.add_argument('--esd-ofile', default="ESDs_to_process.txt", help='Path for output file of ESDs')
    args = parser.parse_args(unparsed_args)
    return args

    if not os.path.isfile(args.ifile_path):
        print( "ERROR :: Unable to find muTrigNt file:", args.muTrigNt_file_path )
        sys.exit()

    return args

def main():
    args = parse_args(sys.argv[1:])

    ESD_muTrigNt_pairs = get_ESD_muTrigNt_pairs(args.ifile_path, args.key)
    
    muTrigNt_pref_list = []
    for _, nts in ESD_muTrigNt_pairs.iteritems():
        if len(nts) == 1:
            muTrigNt_pref_list.append(nts[0])
        else:
            preferred_nts = remove_unwanted_muTrigNts(nts)
            muTrigNt_pref_list += preferred_nts
    
    write_DIDs(ESD_muTrigNt_pairs.keys(), args.esd_ofile)
    write_DIDs(muTrigNt_pref_list, args.muTrigNt_ofile)

def write_DIDs(dids, ofile_path):
    with open(ofile_path, 'w') as ofile:
        ofile.write('\n'.join(sorted(dids)))
    print ("INFO :: Outputs written to", ofile_path)


import json
def get_ESD_muTrigNt_pairs(ifile_path, key):
    d = {}
    # Add ESDs and their matched muTrigNts from input file
    with open(ifile_path) as ifile:
        json_data = json.load(ifile)
        d = json_data[key]

    # Add any ESDs not matched to muTrigNts if manually required 
    if os.path.exists(REQUIRED_ESD_LIST):
        with open(REQUIRED_ESD_LIST, 'r') as ifile:
            for esd in ifile.readlines():
                esd = esd.strip()
                if not esd or esd.startswith('#'):
                    continue
                if esd not in d:
                    d[esd] = []
    return d

def get_scope(did):
    for scope in KNOWN_SCOPES:
        if did.startswith(scope): return scope
    else:
        print ( "WARNING :: Unrecognized scope", did)
        return ''
def rank_scope(did):
    for ii, scope in enumerate(KNOWN_SCOPES):
        if did.startswith(scope): return -ii
    else:
        return -999

import re
def get_ntag(did):
    match = re.search(r'(?<=\.)n[0-9]{3}[_a-zA-Z0-9]*(?=\.)', did)
    if match:
        return match.group()
    else:
        return ""
from string import ascii_lowercase
def rank_ntag(ntag):
    # This function is a hack and handles cases as they arise
    # Should return True if ntag is ranked

    prefix = re.search(r'n[0-9]{3}', ntag).group()
    suffix = ntag.replace(prefix, '')
    if not suffix:
        return 0

    # later alphabet suffix is preferred over non alphabet suffix
    if suffix in ascii_lowercase:
        return list(ascii_lowercase).index(suffix) + 1 

    # beta samples always come before those without beta
    if 'beta' in suffix:
        if suffix.endswith('beta'):
            return -100 
        else:
            return int(re.search(r'(?<=beta)[0-9]*', suffix).group()) - 100

    print ("ERROR :: Unrecognized n-tag (%s). Giving rank = -999" % ntag)
        
from collections import Counter
def get_repeated_ntags(nts):
    ntags = ((re.search(r'n[0-9]{3}', nt).group() for nt in nts))
    return [x for x, c in Counter(ntags).items() if c > 1]

def filter_by_ntag(nts, ntag):
    return [nt for nt in nts if ntag in nt]

def remove_unwanted_muTrigNts(nt_list):
    new_nt_list = nt_list
    repeated_ntags = get_repeated_ntags(new_nt_list)
    unrepeated_ntag_dids = [nt for nt in new_nt_list if not any(t in nt for t in repeated_ntags)]
    # If multiple DIDs with same ntag, use those with preferred scope
    if repeated_ntags:
        tmp_nt_list = []
        for ntag in repeated_ntags:
            ntag_dids = filter_by_ntag(new_nt_list, ntag)
            pref_dids = get_pref_DIDs(ntag_dids, get_scope, rank_scope)
            tmp_nt_list += pref_dids
        new_nt_list = tmp_nt_list
    new_nt_list += unrepeated_ntag_dids    
    new_nt_list = list(set(new_nt_list))

    # If multiple DIDs with same ntag, use those with the latest ntag suffix
    repeated_ntags = get_repeated_ntags(new_nt_list)
    unrepeated_ntag_dids = [nt for nt in new_nt_list if not any(t in nt for t in repeated_ntags)]
    if repeated_ntags:
        tmp_nt_list = []
        for ntag in repeated_ntags:
            ntag_dids = filter_by_ntag(new_nt_list, ntag)
            pref_dids = get_pref_DIDs(ntag_dids, get_ntag, rank_ntag)
            tmp_nt_list += pref_dids
        new_nt_list = tmp_nt_list
    new_nt_list += unrepeated_ntag_dids    
    new_nt_list = list(set(new_nt_list))
    
    # Warn if multiple ntags still exist
    repeated_ntags = get_repeated_ntags(new_nt_list)
    if repeated_ntags:
        print ( "WARNING :: Unable to reduce muTrigNts to one DID per ntag:\n","\n".join(new_nt_list))

    # Return final DIDs
    return new_nt_list

def get_pref_DIDs(dids, prop_func, rank_func):
    props = {d : prop_func(d) for d in dids}
    pref_prop = max(props.values(), key=rank_func)
    return [x for x in props if props[x] == pref_prop]


if __name__ == "__main__":
    main()
