#!/bin/bash

SOURCE_DIR="/data/uclhc/uci/user/armstro1/MuTrigNt/muTrigNtSampleStatus"
OFILE="${SOURCE_DIR}/update_info.log"
export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
export PYTHONPATH="$PYTHONPATH:/home/alarmstr/LexTools"

function update_info {
    echo -e "\n\n"
    echo -e "########################################"
    echo -e "$(date)\n"

    INITIAL_DIR=$PWD
    echo "INFO :: Starting in $PWD"
    cd $SOURCE_DIR
    echo "INFO :: Moving to $PWD"

    echo "INFO :: Setting up ATLAS software and credentials"
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh 
    echo "50.Red.Apple@gr" | voms-proxy-init -voms atlas -valid 96:00
    echo "Setting up needed apps"
    lsetup rucio
    lsetup python
    lsetup "lcgenv -p LCG_90 x86_64-slc6-gcc62-opt pandas"

    echo "INFO :: Getting lists of muTrigNts and ESDs"
    ./scripts/get_all_muTrigNts.sh
    ./scripts/get_all_ESDs.sh

    echo "INFO :: Pairing up muTrigNts and ESDs"
    rm data/ESD_muTrigNt_pairs.json
    ./scripts/pair_ESDs_and_muTrigNts.py data/muTrigNts_all.txt data/ESDs_all.txt -o data/ESD_muTrigNt_pairs.json

    echo "INFO :: Making lists for getting rucio information"
    ./scripts/make_ESD_and_muTrigNt_lists_from_pairs.py data/ESD_muTrigNt_pairs.json --key ESD_muTrigNt_pairs --muTrigNt-ofile data/muTrigNts_to_process.txt --esd-ofile data/ESDs_to_process.txt

    echo "INFO :: Getting info on muTrigNts"
    if [ -f "data/muTrigNt_info.txt" ]; then rm data/muTrigNt_info.txt; fi
    python -u ./scripts/get_dataset_info.py data/muTrigNts_to_process.txt -o data/muTrigNt_info.txt -u GB --mutrignt

    echo "INFO :: Getting info on ESDs"
    if [ -f "data/ESD_info.txt" ]; then rm data/ESD_info.txt; fi
    python -u ./scripts/get_dataset_info.py data/ESDs_to_process.txt -o data/ESD_info.txt -u TB --mutrignt

    echo "INFO :: Storing empty muTrigNts in muTrigNts_empty.txt"
    grep "files : 0" -B1 data/muTrigNt_info.txt | grep -Po "(?<=SAMPLE:\ ).*" > muTrigNts_empty.txt
 
    echo "INFO :: Returning to $INITIAL_DIR"
    cd $INITIAL_DIR
}

update_info >> ${OFILE}.tmp 2>&1
if [ -f "$OFILE" ]; then
    cat $OFILE >> ${OFILE}.tmp
fi
mv ${OFILE}.tmp $OFILE

