#!/bin/bash

################################################################################
# Configuration
################################################################################
OFILE="data/ESDs_all.txt"

PATTERNS=""
PATTERNS="$PATTERNS mc15_14TeV:*recon.ESD*"
PATTERNS="$PATTERNS data16_13TeV:*physics_ZeroBias.recon.ESD*"
PATTERNS="$PATTERNS data16_13TeV:*physics_Main.merge.DESDM_MCP*"
PATTERNS="$PATTERNS data17_13TeV:*physics_Main.merge.DESDM_MCP*"
PATTERNS="$PATTERNS data17_13TeV:*physics_Main.recon.DESDM_ZMUMU*"

################################################################################
# Main
################################################################################
# clear old ofile
> $OFILE

# get listed ESDs
for pattern in $PATTERNS; do
    echo "Getting ESDs for pattern $pattern"
    rucio ls ${pattern} --short | grep -vE "_tid[0-9]{8}_00"| grep -vE "_sub[0-9]{7}" | sort > tmp.txt 
    num_nts=($(wc -l tmp.txt))
    echo "  >> $num_nts result(s) found"
    cat tmp.txt >> ${OFILE}
    rm tmp.txt
done
echo "Output written to $OFILE"
