#!/bin/bash

################################################################################
# Configuration
################################################################################
OFILE="data/muTrigNts_all.txt"

SCOPES=""
SCOPES="$SCOPES group.perf-muons"
SCOPES="$SCOPES user.alarmstr"
SCOPES="$SCOPES user.asoffa"

################################################################################
# Main
################################################################################
# clear old ofile
> $OFILE

# get listed muTrigNts
for scope in $SCOPES; do
    echo "Getting muTrigNts for scope $scope"
    rucio ls ${scope}:*muTrigNt*EXT0 --short --filter type=CONTAINER | grep -vE "_tid[0-9]{8}_00" | sort >> ${OFILE}
    num_nts=($(grep -r "$scope" $OFILE | wc -l))
    echo "  >> $num_nts nTuple(s) found"
done
echo "Output written to $OFILE"
