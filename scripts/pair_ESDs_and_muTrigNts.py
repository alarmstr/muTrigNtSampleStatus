#!/usr/bin/env python

from __future__ import print_function


import sys, os
import argparse

def parse_args(unparsed_args):
    parser = argparse.ArgumentParser()
    parser.add_argument('muTrigNt_file_path', help='Path to input file with muTrigNts')
    parser.add_argument('ESD_file_path', help='Path to input file with ESDs')
    parser.add_argument('-o', '--ofile_path', default="ESD_muTrigNt_pairs.json", help='Path for output file')
    args = parser.parse_args(unparsed_args)

    if not os.path.isfile(args.muTrigNt_file_path):
        print( "ERROR :: Unable to find muTrigNt file:", args.muTrigNt_file_path )
        sys.exit()
    
    if not os.path.isfile(args.ESD_file_path):
        print( "ERROR :: Unable to find ESD file:", args.ESD_file_path )
        sys.exit()
    
    if os.path.exists(args.ofile_path):
        usr_msg =  "Output file already exists: %s\n" % args.ofile_path
        usr_msg += "Would you like to overwrite it? [Y/N] "
        user_op = raw_input(usr_msg)

        # Only accept U or O
        while user_op not in ["Y","N"]:
            usr_msg = "Unacceptable answer: %s\n" % user_op
            usr_msg += "Would you like to overwrite it? [Y/N] "
            user_op = raw_input(usr_msg)

        if user_op == "N":
            print ( "INFO :: Please choose a different output file name or delete the old file" )
            print ( "INFO :: Exiting program" )
            sys.exit()

    return args

from collections import defaultdict
def main():
    args = parse_args(sys.argv[1:])

    # Get lists of muTrigNts and ESDs from input files
    unfiltered_muTrigNts = get_file_text(args.muTrigNt_file_path)
    muTrigNts = unfiltered_muTrigNts
    
    unfiltered_ESDs = get_file_text(args.ESD_file_path)
    ESDs = unfiltered_ESDs

    # Identify muTrigNts with a known ESD
    output_dict = {}
    output_dict['ESD_muTrigNt_pairs'] = defaultdict(list)
    output_dict['matched_muTrigNts'] = []
    output_dict['unmatched_muTrigNts'] = []
    output_dict['ignored_muTrigNts'] = []
    output_dict['ignored_ESDs'] = set()

    for nt in muTrigNts:
        if not acceptable_muTrigNt(nt): 
            output_dict['ignored_muTrigNts'].append(nt)
            continue
        matched_ESD = "" 
        for esd in ESDs:
            if not acceptable_ESD(esd): 
                output_dict['ignored_ESDs'].add(esd)
                continue
            if esd_nt_match(esd, nt):
                if matched_ESD:
                    print ( "WARNING :: muTrigNt matched to multiple ESDs:\n\t%s\n\t%s" % (matched_ESD, esd) )
                else:
                    output_dict['matched_muTrigNts'].append(nt)
                matched_ESD = esd
                output_dict['ESD_muTrigNt_pairs'][esd].append(nt)

        if not matched_ESD:
            output_dict['unmatched_muTrigNts'].append(nt)

    output_dict['unmatched_ESDs'] = [e for e in ESDs if e not in output_dict['ESD_muTrigNt_pairs']]
    output_dict['ignored_ESDs'] = list(output_dict['ignored_ESDs'])

    # Sanity checks
    assert len(unfiltered_muTrigNts) == (len(output_dict['ignored_muTrigNts'])
                                        +len(output_dict['unmatched_muTrigNts'])
                                        +len(output_dict['matched_muTrigNts']))
    assert len(unfiltered_ESDs) == (len(output_dict['ignored_ESDs'])
                                   +len(output_dict['unmatched_ESDs'])
                                   +len(output_dict['ESD_muTrigNt_pairs']))
    # Save output
    save_dict_to_json(args.ofile_path, output_dict)

################################################################################
# Supporting functions
################################################################################
def get_file_text(file_path):
    with open(file_path) as f:
        text = [line.strip() for line in f.readlines()]
    return text

def acceptable_DID(did):
    if not did:
        return False
    elif not get_DSID(did):
        return False
    elif "test" in did:
        return False
    else:
        return True

def acceptable_muTrigNt(muTrigNt):
    if not acceptable_DID(muTrigNt):
        return False
    if "muTrigNt" not in muTrigNt:
        return False
    elif not muTrigNt.endswith("EXT0"):
        return False
    elif not get_tags(muTrigNt, filter_tags=['n010', 'n011']):
        return False
    elif "express_express" in muTrigNt:
        return False
    elif any(x in muTrigNt for x in ['n010_1k', 'n010_50k', 'n010_500k']):
        return False
    else:
        return True

def acceptable_ESD(ESD):
    if not acceptable_DID(ESD):
        return False
    elif "ESD" not in ESD:
        return False
    elif "_tid" in ESD and ESD.endswith('00'):
        return False
    else:
        return True

import re
def get_DSID(did):
    # find sequence of 6 digits starting with a non-zero number followed by a period
    # should only return one match
    result = re.findall(r'[1-9][0-9]{5}(?=\.)', did)
    if len(result) != 1:
        return ""
    else:
        return result[0]

def get_tags(did, exclude_tags=[], filter_tags=[]):
    # find all instances of a single letter, except 'n', followed by at least one number
    result = re.findall(r'(?<=[._])[a-z][0-9]+', did)
    if exclude_tags:
        # select only tags that are not the same as or even contain what is listed in exclude_tags
        result = list(filter(lambda t: all(x not in t for x in exclude_tags), result))
    if filter_tags:
        # select only tags that are the same as or contain what is listed in at least one entry of filter_tags
        result = list(filter(lambda t: any(x in t for x in filter_tags), result))
    return result
    

def esd_nt_match(esd, nt):
    if get_DSID(nt) != get_DSID(esd):
        return False
    elif get_tags(nt, exclude_tags=['n']) != get_tags(esd, exclude_tags=['n']):
        return False
    else:
        return True

import json
def save_dict_to_json(ofile_path, dic):
    with open(ofile_path, 'w') as ofile:
        json_txt = json.dump(dic, ofile, sort_keys=True, indent=4)
    print ( "INFO :: Matches stored in", ofile_path)


if __name__ == '__main__':
    main()
