# File Description
- [`data/muTrigNts_all.txt`](data/muTrigNts_all.txt) - All muTrigNts found on rucio using [`scripts/get_all_muTrigNts.sh`](scripts/get_all_muTrigNts.sh).
- [`data/ESDs_all.txt`](data/ESDs_all.txt) - All ESDs considered when looking for matches to muTrigNts (retrieved using [`scripts/get_all_ESDs.sh`](scripts/get_all_ESDs.sh)).
- [`data/ESD_muTrigNt_pairs.json`](data/ESD_muTrigNt_pairs.json) - Lists of muTrigNts and their parent ESDs as well as any ESDs or muTrigNts that were unmatched or ignored. Samples are ignored if they do not pass the `acceptable_X` functions defined in [`scripts/pair_ESDs_and_muTrigNts.py`](scripts/pair_ESDs_and_muTrigNts.py).
- [`data/muTrigNt_info.txt`](data/muTrigNt_info.txt) and [`data/ESD_info.txt`](data/ESD_info.txt) - Metadata on select ESDs and muTrigNts. All ESDs paired to a muTrigNt are used and all the muTrigNts paired to an ESD and passing the `remove_unwanted_muTrigNts` function defined in [`scripts/make_ESD_and_muTrigNt_lists_from_pairs.py`](scripts/make_ESD_and_muTrigNt_lists_from_pairs.py) are used.
- [`data/muTrigNts_delete.txt`](data/muTrigNts_delete.txt) - samples that should be deleted from the GRID. These files are all empty or unused

# Commands
Run `./scripts/update_info.sh` to update sample info. It can take O(hour) to get the rucio metadata on all muTrigNts and ESDs. The stdout is stored in the file `update_info.log`

Other commands to consider:
- `cp data/ESD_muTrigNt_pairs.json muTrigNts_delete.txt` and remove all but the ignored and unmatched muTrigNts from `muTrigNts_delete.txt`. Consider requesting these samples be deleted
- Use csv files in `data/` to update google sheet (https://docs.google.com/spreadsheets/d/15tYI1HHsPaXV54MS37RqZlJNQuXKpX0rxXNnSHxl8AQ/edit#gid=1526379585)

TODO:
- Automate the uploading of sample info to the Google Sheet using GoogleSheetAPI
- Write a script that submits condor jobs to access important files in as low-cost way to prompt caching
    - Keep a list of samples to be processed by this script
